import { createMuiTheme } from "material-ui/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#b9dd6e",
      main: "#87ab3f",
      dark: "#577c09",
      contrastText: "#e3e3e3"
    },
    secondary: {
      light: "#595959",
      main: "rgba(70, 70, 70, 0.9)",
      dark: "#303030",
      contrastText: "#fff"
    }
  }
});

export default theme;