import React from "react";
import PropTypes from "prop-types";
import { Typography, List, ListItem } from "material-ui";

const Navigation = ({ components }) => (
  <div style={{ padding: "20px", width: "300px", height: "100vh", overflowY: "auto" }}>
    <Typography variant="headline">Components</Typography>
    <List dense={true}>
      {components.map(name => (
        <ListItem key={name}>
          <a href={`#${name}`}>{name}</a>
        </ListItem>
      ))}
    </List>
  </div>
);

Navigation.propTypes = {
  components: PropTypes.array.isRequired
};

export default Navigation;
