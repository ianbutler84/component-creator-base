import React from "react";
import PropTypes from "prop-types";
import { Collapse, Divider, Card } from "material-ui";

import CodeExample from "./CodeExample";

class Example extends React.Component {
  state = { showCode: false };

  toggleCode = event => {
    event.preventDefault();
    this.setState(prevState => ({ showCode: !prevState.showCode }));
  };

  render() {
    const { showCode } = this.state;
    const { code, description, name } = this.props.example;

    // Must use CommonJS require to dynamically require because ES Modules must be statically analyzable.
    const ExampleComponent = require(`./examples/${this.props.componentName}/${name}`).default;

    return (
      <React.Fragment>
        <Card className="example">
          {description && <h4 style={{ paddingBottom: "20px" }}>{description}</h4>}
          <ExampleComponent />
          <Divider style={{ marginTop: "20px", marginBottom: "20px" }} />
          <div>
            <a style={{ cursor: "pointer" }} onClick={this.toggleCode}>
              <code>{showCode ? "Hide" : "Show"} Code</code>
            </a>
          </div>
          <Collapse style={showCode ? { marginTop: "20px" } : {}} in={showCode} timeout="auto">
            <CodeExample>{code}</CodeExample>
          </Collapse>
        </Card>
      </React.Fragment>
    );
  }
}

Example.propTypes = {
  example: PropTypes.object.isRequired,
  componentName: PropTypes.string.isRequired
};

export default Example;
