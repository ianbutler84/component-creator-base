import React from "react";
import PropTypes from "prop-types";
import SyntaxHighlighter, { registerLanguage } from "react-syntax-highlighter/prism-light";
import jsx from "react-syntax-highlighter/languages/prism/jsx";
import atomDark from "react-syntax-highlighter/styles/prism/tomorrow";

class CodeExample extends React.Component {
  componentDidMount() {
    registerLanguage("jsx", jsx);
  }

  render() {
    return (
      <pre
        ref={ref => {
          this.element = ref;
        }}
      >
        <code>
          <SyntaxHighlighter language="jsx" style={atomDark}>
            {this.props.children}
          </SyntaxHighlighter>
        </code>
      </pre>
    );
  }
}

CodeExample.propTypes = {
  children: PropTypes.string.isRequired
};

export default CodeExample;
