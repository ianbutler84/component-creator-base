import React from "react";
import { NavLink } from "react-router-dom";
import AddIcon from "material-ui-icons/Add";
import Sidebar from "ps-react/Sidebar";

/** The sidebar below has 2 sections.
 * The first has an action button associated with it.
 * The second section has nesting. */
const SidebarExample = () => (
  <div style={{ padding: "20px", backgroundColor: "#eee", width: "300px", height: "500px" }}>
    <Sidebar
      headline="Sidebar Example"
      contentGroup={[
        {
          subTitle: "Subtitle",
          action: {
            icon: <AddIcon />,
            action: () => {
              console.log("Action fired");
            }
          },
          list: [
            { title: "Boom", to: "/boom", component: NavLink },
            { title: "2nd Example", to: "/second", component: NavLink },
            { title: "3rd Example", to: "/third", component: NavLink }
          ]
        },
        {
          subTitle: "2nd section",
          list: [
            {
              title: "Boom",
              component: NavLink,
              to: "/boom2",
              list: [
                { title: "nested item 1", component: NavLink, to: "/boom2/nestA" },
                { title: "nested item 2", component: NavLink, to: "/boom2/nestB" },
                { title: "nested item 3", component: NavLink, to: "/boom2/nestC" }
              ]
            },
            {
              title: "2nd Example",
              component: NavLink,
              to: "/second2"
            },
            {
              title: "3rd Example",
              component: NavLink,
              to: "/third2",
              list: [
                { title: "nested item d", component: NavLink, to: "/third2/nestD" },
                { title: "nested item e", component: NavLink, to: "/third2/nestE" }
              ]
            }
          ]
        }
      ]}
    />
  </div>
);

export default SidebarExample;
