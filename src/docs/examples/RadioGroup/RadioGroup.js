import React, { Component } from "react";
import RadioGroup from "ps-react/RadioGroup";

/** RadioGroup needs an array of objects, each with a value and a label */
class RadioGroupExample extends Component {
  state = { radioInput: "" };

  handleChange = (name, val) => {
    this.setState({ [name]: val });
  };

  render() {
    return (
      <div>
        <div>
          <RadioGroup
            name="radioInput"
            handleChange={this.handleChange}
            title="Pick something"
            radioArr={[
              { value: "One", label: "One" },
              { value: "Two", label: "Two" },
              { value: "dasfasdf", label: "aasdfsdf" }
            ]}
          />
        </div>
        <div>Current Value: {this.state.radioInput}</div>
      </div>
    );
  }
}

export default RadioGroupExample;
