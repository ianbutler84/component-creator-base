import React, { Component } from "react";
import Input from "ps-react/Input";

/** Standard Input example */
class InputExample extends Component {
  state = { inputDescription1: "", inputDescription2: "" };

  handleChange = (name, val) => {
    this.setState({ [name]: val });
  };

  render() {
    const { inputDescription1, inputDescription2 } = this.state;

    return (
      <div style={{ width: "200px" }}>
        <div>
          <Input name="inputDescription1" handleChange={this.handleChange} label="InputDescription1" />
          {`Input1 state: ${inputDescription1}`}
        </div>
        <div>
          <Input name="inputDescription2" handleChange={this.handleChange} label="InputDescription2" />
          {`Input1 state: ${inputDescription2}`}
        </div>
      </div>
    );
  }
}

export default InputExample;
