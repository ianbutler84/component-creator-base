import React, { Component } from "react";
import Radio from "ps-react/Radio";

/** Basic radio example */
class RadioExample extends Component {
  state = {
    checked: false
  };

  toggleChecked = () => {
    this.setState(prevState => ({
      checked: !prevState.checked
    }));
  };

  render() {
    return (
      <form>
        <Radio handleChange={this.toggleChecked} checked={this.state.checked} />
        <Radio handleChange={this.toggleChecked} checked={!this.state.checked} />
      </form>
    );
  }
}

export default RadioExample;
