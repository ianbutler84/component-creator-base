import React, { Component } from "react";
import Select from "ps-react/Select";

/** Select needs an array of objects, each with a value and a label */
class SelectExample extends Component {
  state = { inputValue: "" };

  handleChange = (name, val) => {
    this.setState({ [name]: val });
  };

  render() {
    return (
      <div>
        <div>
          <Select
            name="inputValue"
            handleChange={this.handleChange}
            title="Pick something"
            minWidth={140}
            selectList={[
              { value: "One", label: "One" },
              { value: "Two", label: "Two" },
              { value: "dasfasdf", label: "aasdfsdf" }
            ]}
          />
        </div>
        <div>Select Value: {this.state.inputValue}</div>
      </div>
    );
  }
}

export default SelectExample;
