import React, { Component } from "react";
import Stepper from "ps-react/Stepper";
import { Button } from "material-ui";

/** Below is an example of how you would implement a stepper */
class StepperExample extends Component {
  state = {
    currentStep: 1,
    steps: 4
  };
  stepForward = () => {
    const { currentStep, steps } = this.state;
    if (currentStep >= steps) return;
    this.setState(prevState => ({ currentStep: prevState.currentStep + 1 }));
  };

  stepBack = () => {
    const { currentStep } = this.state;
    if (currentStep <= 1) return;
    this.setState(prevState => ({ currentStep: prevState.currentStep - 1 }));
  };

  clickOnNumber = i => {
    console.log(`index: ${i}`);
    this.setState({ currentStep: i + 1 });
  };

  render() {
    const { currentStep, steps } = this.state;
    return (
      <div>
        <Stepper specificStepAction={this.clickOnNumber} currentStep={currentStep} steps={steps} />
        <div style={{ paddingTop: "20px" }}>
          <Button onClick={this.stepBack}>Step Back</Button>
          <Button onClick={this.stepForward}>Step Forward</Button>
        </div>
      </div>
    );
  }
}

export default StepperExample;
