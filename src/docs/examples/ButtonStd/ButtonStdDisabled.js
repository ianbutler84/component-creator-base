import React from "react";
import ButtonStd from "ps-react/ButtonStd";

const ButtonExample = () => <ButtonStd disabled={true}>Click</ButtonStd>;

export default ButtonExample;
