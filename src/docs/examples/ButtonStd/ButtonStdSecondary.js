import React from "react";
import ButtonStd from "ps-react/ButtonStd";

const ButtonExample = () => <ButtonStd color="secondary">Secondary</ButtonStd>;

export default ButtonExample;
