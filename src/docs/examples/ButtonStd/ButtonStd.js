import React from "react";
import ButtonStd from "ps-react/ButtonStd";

const ButtonExample = () => <ButtonStd>Click</ButtonStd>;

export default ButtonExample;