import React from "react";
import NavMenuNest from "ps-react/NavMenuNest";

const NavMenuNestExample = () => (
  <div style={{ width: "200px", height: "400px", border: "1px solid rgba(0,0,0, 0.3)" }}>
    <NavMenuNest
      expanded={true}
      listItem={{
        title: "Boom",
        list: [{ title: "nested item 1" }, { title: "nested item 2" }, { title: "nested item 3" }]
      }}
    />
  </div>
);

export default NavMenuNestExample;
