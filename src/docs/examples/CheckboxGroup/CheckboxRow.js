import React from "react";
import CheckboxGroup from "ps-react/CheckboxGroup";

/** CheckboxGroup with row={true} */
const CheckboxGroupExample = () => (
  <CheckboxGroup
    row={true}
    title="Pick something else"
    checkboxArr={[
      { value: "One", label: "One" },
      { value: "Two", label: "Two" },
      { value: "dasfasdf", label: "aasdfsdf" }
    ]}
  />
);

export default CheckboxGroupExample;
