import React, { Component } from "react";
import CheckboxGroup from "ps-react/CheckboxGroup";

/** CheckboxGroup needs an array of objects, each with a value and a label */
class CheckboxGroupExample extends Component {
  state = {
    cbArr: [{ value: false, label: "One" }, { value: false, label: "Two" }, { value: false, label: "aasdfsdf" }]
  };

  handleChange = (name, val) => {
    console.log("Boom! name: ", name);
    console.log("Boom!: val:", val);
  };

  render() {
    return (
      <CheckboxGroup
        name="checkboxEx1"
        handleChange={this.handleChange}
        title="Pick something"
        checkboxArr={this.state.cbArr}
      />
    );
  }
}

export default CheckboxGroupExample;
