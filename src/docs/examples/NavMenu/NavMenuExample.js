import React from "react";
import { NavLink } from "react-router-dom";
import AddIcon from "material-ui-icons/Add";
import NavMenu from "ps-react/NavMenu";

/** This is a menu with each menu item being a link */
const NavMenuExample = () => (
  <div style={{ width: "200px", border: "1px solid rgba(0,0,0, 0.3)" }}>
    <NavMenu
      lastGroup={true}
      group={{
        action: {
          icon: <AddIcon />,
          action: () => {
            console.log("Action fired");
          }
        },
        subTitle: "Subtitle",
        list: [
          { title: "nested item 1", component: NavLink, to: "/nestA" },
          { title: "nested item 2", component: NavLink, to: "/nestB" },
          { title: "nested item 3", component: NavLink, to: "/nestC" }
        ]
      }}
    />
  </div>
);

export default NavMenuExample;
