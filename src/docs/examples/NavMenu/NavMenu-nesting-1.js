import React from "react";
import NavMenu from "ps-react/NavMenu";

/** This menu has nesting.  All menu items are a div, so there are no links associated */
const NavMenuNesting = () => (
  <div style={{ width: "200px", border: "1px solid rgba(0,0,0, 0.3)" }}>
    <NavMenu
      group={{
        component: "div",
        subTitle: "Subtitle",
        list: [
          {
            title: "Boom",
            list: [{ title: "nested item 1" }, { title: "nested item 2" }, { title: "nested item 3" }]
          },
          { title: "2nd Example" },
          { title: "3rd Example", list: [{ title: "nested item a" }, { title: "nested item a" }] }
        ]
      }}
    />
  </div>
);

export default NavMenuNesting;
