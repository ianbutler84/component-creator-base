import React from "react";
import LoadingButton from "ps-react/LoadingButton";
import withState from "recompose/withState";

const LoadingButtonToggle = withState('isLoading', 'toggleLoading', false)(({isLoading, toggleLoading}) => (
  <div>
    <LoadingButton
      variant="raised"
      color="primary"
      isLoading={isLoading}
      onClick={() => console.log("Clickty click")}
    >
      Really Long Text Button
    </LoadingButton>

    <LoadingButton
      style={{marginLeft: 20}}
      onClick={() => toggleLoading(!isLoading)}
    >
      Toggle
    </LoadingButton>
  </div>

));



export default LoadingButtonToggle;
