import React from "react";
import LoadingButton from "ps-react/LoadingButton";

const LoadingButtonNotLoading = () => (
  <LoadingButton
    onClick={() => {
      console.log("Click");
    }}
    variant="raised"
    color="primary"
    isLoading={false}
  >
    Not loading
  </LoadingButton>
);

export default LoadingButtonNotLoading;
