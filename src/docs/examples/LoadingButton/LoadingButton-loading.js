import React from "react";
import LoadingButton from "ps-react/LoadingButton";

const LoadingButtonNotLoading = () => (
  <LoadingButton
    onClick={() => {
      console.log("You won't get a log... it's disabled");
    }}
    variant="raised"
    color="primary"
    isLoading={true}
  >
    Loading
  </LoadingButton>
);

export default LoadingButtonNotLoading;
