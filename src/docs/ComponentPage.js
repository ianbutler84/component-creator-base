import React from "react";
import PropTypes from "prop-types";
import { Typography, Paper } from "material-ui";

import Example from "./Example";
import Props from "./Props";

const styles = { paddingTop: "30px" };

const ComponentPage = ({ component }) => {
  const { name, description, props, examples } = component;

  return (
    <div style={{ overflowY: "auto", height: "100vh", flexGrow: "1", backgroundColor: "#eee", padding: "30px" }}>
      <Typography variant="display3">&lt;{name} /&gt;</Typography>
      <p>{description}</p>

      <div style={styles}>
        <Typography style={{ paddingBottom: "10px" }} variant="display1">
          Example{examples.length > 1 && "s"}
        </Typography>
        {examples.length > 0
          ? examples.map(example => <Example key={example.code} example={example} componentName={name} />)
          : "No examples exist."}
      </div>

      <div style={styles}>
        <Paper style={{ padding: "20px" }}>
          <Typography variant="display1">Props</Typography>
          {props ? <Props props={props} /> : "This component accepts no props"}
        </Paper>
      </div>
    </div>
  );
};

ComponentPage.propTypes = { component: PropTypes.object.isRequired };

export default ComponentPage;
