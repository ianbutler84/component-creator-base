import React, { Component } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { MuiThemeProvider } from "material-ui/styles";

import Navigation from "./Navigation";
import ComponentPage from "./ComponentPage";
import componentData from "../../config/componentData";
import theme from "../util/theme";

export default class Docs extends Component {
  state = {
    route: window.location.hash.substr(1)
  };

  componentDidMount() {
    window.addEventListener("hashchange", () => {
      this.setState({ route: window.location.hash.substr(1) });
    });
  }

  render() {
    const { route } = this.state;
    const component = route ? componentData.filter(component => component.name === route)[0] : componentData[0];

    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          <div style={{ display: "flex" }}>
            <Navigation components={componentData.map(component => component.name)} />
            <ComponentPage component={component} />
          </div>
        </Router>
      </MuiThemeProvider>
    );
  }
}
