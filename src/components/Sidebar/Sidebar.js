import React from "react";
import PropTypes from "prop-types";
import NavMenu from "../NavMenu";
import { Typography } from "material-ui";

/** Description: */
const Sidebar = ({ headline, contentGroup }) => (
  <React.Fragment>
    <div style={{ backgroundColor: "#fff", overflowY: "auto", overflowX: "hidden" }}>
      <Typography style={{ paddingLeft: "15px" }} variant="headline">
        {headline}
      </Typography>
      {contentGroup.length &&
        contentGroup.map((group, i) => (
          <NavMenu lastGroup={i === contentGroup.length - 1 ? true : false} group={group} key={i} />
        ))}
    </div>
  </React.Fragment>
);

Sidebar.propTypes = {
  /** contentGroup will be a formatted Array  */
  contentGroup: PropTypes.arrayOf(
    PropTypes.shape({
      subTitle: PropTypes.string,
      list: PropTypes.array.isRequired
    })
  ).isRequired,
  /** headline will be the title given to the sidebar */
  headline: PropTypes.string.isRequired
};

Sidebar.defaultProps = {};

export default Sidebar;
