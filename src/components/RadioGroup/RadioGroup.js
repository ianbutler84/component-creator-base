import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles, RadioGroup as RadioGroupMUI, Radio, FormControl, FormLabel, FormControlLabel } from "material-ui";

const styles = theme => ({});

/** RadioGroup */
class RadioGroup extends Component {
  state = {
    radioVal: this.props.radioArr[0].value
  };

  handleChange = e => {
    const newValue = e.target.value;
    this.setState({ radioVal: newValue }, () => this.props.handleChange(this.props.name, newValue));
  };

  render() {
    const { radioArr, title, row, name } = this.props;
    const { radioVal } = this.state;

    return (
      <FormControl component="fieldset">
        <FormLabel style={{ marginBottom: "10px" }} component="legend">
          {title}
        </FormLabel>
        <RadioGroupMUI row={row} style={{ display: "flex" }} value={radioVal} name={name} onChange={this.handleChange}>
          {radioArr.length &&
            radioArr.map((item, i) => (
              <FormControlLabel
                key={i}
                value={item.value}
                control={<Radio style={{ height: "30px" }} color="primary" />}
                label={item.label}
              />
            ))}
        </RadioGroupMUI>
      </FormControl>
    );
  }
}

RadioGroup.propTypes = {
  /** radioArr will need to be an array of objects, containing both a value and a label property */
  radioArr: PropTypes.array.isRequired,
  /** This is for the title/name of the radiogroup */
  title: PropTypes.string.isRequired,
  /** Boolean allowing you to decide whether you want the radio buttons displayed in a row or horizontally */
  row: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired
};

RadioGroup.defaultProps = {
  row: false
};

export default withStyles(styles)(RadioGroup);
