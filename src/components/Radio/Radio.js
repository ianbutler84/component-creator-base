import React from "react";
import PropTypes from "prop-types";
import { Radio as RadioMUI, withStyles } from "material-ui";

const styles = theme => ({});

/** Radio Button */
const Radio = ({ handleChange, checked }) => <RadioMUI onChange={handleChange} checked={checked === true} />;

Radio.propTypes = {
  /** function that will alter which radio is selected */
  handleChange: PropTypes.func,
  /** boolean value deciding whether radio is selected or not */
  checked: PropTypes.bool.isRequired
};

Radio.defaultProps = {};

export default withStyles(styles)(Radio);
