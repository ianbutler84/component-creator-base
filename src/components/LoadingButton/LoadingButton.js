import React from "react";
import PropTypes from "prop-types";
import { CircularProgress, Button } from "material-ui";

/** A button that will show a loading spinner while waiting for API call results so it isn't clicked a 2nd time when not ready for it */
class LoadingButton extends React.Component {
  state = {
    minWidth: 0
  };

  constructor(props) {
    super(props);
    this.buttonRef = React.createRef();
  }

  componentDidMount() {
    const minWidth = this.buttonRef.getBoundingClientRect().width;

    this.setState({ minWidth });
  }

  render() {
    const { minWidth } = this.state;
    const { isLoading, loadingSize, children, onClick, ...props } = this.props;

    return (
      <Button buttonRef={button => (this.buttonRef = button)} style={{ minWidth }} onClick={isLoading ? () => {} : onClick} {...props}>
        {isLoading ? <CircularProgress size={loadingSize} style={{ color: "inherit" }} /> : children}
      </Button>
    );
  }
}

LoadingButton.propTypes = {
  /** Determines whether you see the loading spinner or not */
  isLoading: PropTypes.bool,
  /** Determines the size of the loading spinner */
  loadingSize: PropTypes.number,
  /** Click handler function */
  onClick: PropTypes.func
};

LoadingButton.defaultProps = {
  isLoading: false,
  loadingSize: 25
};

export default LoadingButton;
