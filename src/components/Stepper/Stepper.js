import React from "react";
import PropTypes from "prop-types";
import { Typography } from "material-ui";

/** Stepper allows you to go through a secuence of steps */
const Stepper = ({ steps, currentStep, specificStepAction }) => (
  <div style={{ display: "flex", flexWrap: "nowrap", justifyContent: "center", alignItems: "center" }}>
    {Array(steps)
      .fill()
      .map((step, i) => (
        <div key={i} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
          {i !== 0 && (
            <div
              style={{
                height: "5px",
                width: "30px",
                backgroundColor: currentStep > i ? "#87ab3f" : "#bbb",
                transition: "background-color 0.5s"
              }}
            />
          )}
          <div
            onClick={() => {
              specificStepAction(i);
            }}
            style={{
              borderRadius: "50%",
              color: "#fff",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "30px",
              height: "30px",
              cursor: "pointer",
              transition: "background-color 0.5s",
              backgroundColor: currentStep > i ? "#87ab3f" : "#bbb"
            }}
          >
            <Typography style={{ color: "#fff", fontWeight: "bold" }}>{i + 1}</Typography>
          </div>
        </div>
      ))}
  </div>
);

Stepper.propTypes = {
  /** The amount of steps in your process */
  steps: PropTypes.number.isRequired,
  /** The active step you are currently on */
  currentStep: PropTypes.number.isRequired,
  /** You can pass a function in that fires when you click on a number */
  specificStepAction: PropTypes.func
};

Stepper.defaultProps = {
  specificStepAction: () => {},
  currentStep: 1
};

export default Stepper;
