import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles, Select as SelectMUI } from "material-ui";
import { FormControl } from "material-ui/Form";
import { MenuItem } from "material-ui/Menu";
import { InputLabel } from "material-ui/Input";

const styles = theme => ({});

/** Select input dropdown */
class Select extends Component {
  state = { selectValue: "" };

  handleChange = e => {
    const newValue = e.target.value;
    this.setState({ selectValue: e.target.value }, () => this.props.handleChange(this.props.name, newValue));
  };

  render() {
    const { selectValue } = this.state;
    const { selectList, title, name, minWidth, shrink } = this.props;

    return (
      <FormControl style={{ minWidth: minWidth }}>
        {shrink ? <InputLabel shrink={shrink}>{title}</InputLabel> : <InputLabel>{title}</InputLabel>}
        <SelectMUI name={name} inputProps={{ name: "selectValue" }} value={selectValue} onChange={this.handleChange}>
          {selectList.length &&
            selectList.map((option, index) => (
              <MenuItem name={option.label} key={index} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
        </SelectMUI>
      </FormControl>
    );
  }
}

Select.propTypes = {
  /** This will be the title of the input */
  title: PropTypes.string.isRequired,
  /** selectList will be an array of objects, all with a label and a value property */
  selectList: PropTypes.array.isRequired,
  /** minWidth is the minimum width of the select input.  default set at 120 */
  minWidth: PropTypes.number,
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  shrink: PropTypes.bool
};

Select.defaultProps = {
  minWidth: 120,
  shrink: false
};

export default withStyles(styles)(Select);
