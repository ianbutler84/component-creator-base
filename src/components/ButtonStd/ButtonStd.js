import React from "react";
import PropTypes from "prop-types";
import { Button } from "material-ui";

/** A sample component that uses Material UI button */
const ButtonStd = ({ color, variant, disabled, children }) => (
  <Button disabled={disabled} variant={variant} color={color}>
    {children}
  </Button>
);

ButtonStd.propTypes = {
  /** color uses the MUI theme designated colors  */
  color: PropTypes.string,
  /** variant is a MUI option deciding between flat, raised, and other options */
  variant: PropTypes.string,
  /** Allows you to disable the button from being used */
  disabled: PropTypes.bool
};

ButtonStd.defaultProps = {
  variant: "raised",
  color: "primary",
  disabled: false
};

export default ButtonStd;
