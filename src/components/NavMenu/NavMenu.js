import React, { Component } from "react";
import PropTypes from "prop-types";
import { List, ListSubheader, ListItem, ListItemText, withStyles, Divider } from "material-ui";
import ExpandMoreIcon from "material-ui-icons/ExpandMore";
import ExpandLessIcon from "material-ui-icons/ExpandLess";
import NavMenuNest from "../NavMenuNest";

const styles = theme => {
  const activeBorderWidth = 5;
  return {
    listItem: {
      padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
      "&.active": {
        borderLeft: `solid ${activeBorderWidth}px ${theme.palette.primary.main}`,
        paddingLeft: theme.spacing.unit * 2 - activeBorderWidth
      }
    }
  };
};

/** Description: A menu with a title.  originally built to be a component of the sidebar*/
class NavMenu extends Component {
  state = {
    expanded: 0
  };

  expandMenu = i => {
    const val = i === this.state.expanded ? false : i;
    this.setState({ expanded: val });
  };

  render() {
    const { group, lastGroup, classes } = this.props;
    const { expanded } = this.state;

    return (
      <React.Fragment>
        <List
          component={group.component || "div"}
          subheader={
            group.subTitle && (
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <ListSubheader disableSticky={true} component="div">
                  {group.subTitle}
                </ListSubheader>
                {group.action && group.action.icon}
              </div>
            )
          }
        >
          {group.list.length &&
            group.list.map((listItem, i) => (
              <React.Fragment key={i}>
                <ListItem
                  ref={i}
                  onClick={() => this.expandMenu(i)}
                  button
                  className={classes.listItem}
                  component={listItem.component || "div"}
                  to={listItem.to || "/"}
                >
                  <div
                    style={{ width: "100%", alignItems: "center", display: "flex", justifyContent: "space-between" }}
                  >
                    <ListItemText primary={listItem.title} />
                    {listItem.list && expanded === i && <ExpandMoreIcon />}
                    {listItem.list && expanded !== i && <ExpandLessIcon />}
                  </div>
                </ListItem>

                {listItem.list && (
                  <NavMenuNest
                    componentType={listItem.component || "div"}
                    to={listItem.to || "/"}
                    expanded={expanded === i}
                    listItem={listItem}
                  />
                )}
              </React.Fragment>
            ))}
        </List>
        {!lastGroup && <Divider />}
      </React.Fragment>
    );
  }
}

NavMenu.propTypes = {
  /** Group is an object with a title, and possibly a list that will be nested */
  group: PropTypes.shape({
    list: PropTypes.array,
    title: PropTypes.string,
    /** This could either be a string or a navLink component */
    component: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    /** Link if you're using a NavLink component */
    to: PropTypes.string,
    /** The action prop will have an icon you want to display and a function to fire when you click that action */
    action: PropTypes.shape({
      icon: PropTypes.element,
      action: PropTypes.func
    })
  }),
  /** The last group in the array will not have a Divider below it */
  lastGroup: PropTypes.bool
};

NavMenu.defaultProps = {};

export default withStyles(styles)(NavMenu);
