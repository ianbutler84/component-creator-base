import React from "react";
import PropTypes from "prop-types";
import { Typography, withStyles, List, Collapse, ListItem } from "material-ui";

const styles = theme => {
  return {
    subList: {
      padding: 0
    },
    subListItem: {
      padding: `${theme.spacing.unit / 3}px ${theme.spacing.unit * 4}px`
    },
    subListItemText: {
      color: theme.palette.grey[600],
      "&.active": {
        color: theme.palette.primary.main
      }
    }
  };
};

/** Description: */
const NavMenuNest = ({ listItem, classes, expanded }) => (
  <Collapse in={expanded} timeout="auto" unmountOnExit>
    <List className={classes.subList}>
      {listItem.list.map((nestedItem, i) => (
        <ListItem
          component={nestedItem.component || "div"}
          to={nestedItem.to || ""}
          key={i}
          className={classes.subListItem}
        >
          <Typography variant="body1">{nestedItem.title}</Typography>
        </ListItem>
      ))}
    </List>
  </Collapse>
);

NavMenuNest.propTypes = {
  listItem: PropTypes.shape({
    title: PropTypes.string
  }),
  /** Boolean determining whether menu is expanded or not */
  expanded: PropTypes.bool.isRequired
};

NavMenuNest.defaultProps = {
  expanded: false
};

export default withStyles(styles)(NavMenuNest);
