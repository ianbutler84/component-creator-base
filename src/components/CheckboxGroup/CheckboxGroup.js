import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles, FormGroup, Checkbox, FormControl, FormLabel, FormControlLabel } from "material-ui";

const styles = theme => ({});

/** CheckboxGroup */
class CheckboxGroup extends Component {
  state = { checkboxArr: [] };

  componentDidMount() {
    this.setState({ checkboxArr: this.props.checkboxArr });
  }

  onChange = val => {
    this.setState(
      prevProps => ({ [val]: !prevProps[val] }),
      () => {
        this.props.handleChange(this.props.name, "alot");
      }
    );
  };

  render() {
    const { checkboxArr, title, row } = this.props;

    return (
      <FormControl component="fieldset">
        <FormLabel style={{ marginBottom: "10px" }} component="legend">
          {title}
        </FormLabel>
        <FormGroup row={row} style={{ display: "flex" }}>
          {checkboxArr.length &&
            checkboxArr.map((item, i) => (
              <FormControlLabel
                key={i}
                control={
                  <Checkbox
                    checked={this.state[item.value] === true}
                    style={{ height: "30px" }}
                    color="primary"
                    onChange={() => {
                      this.onChange(item.value);
                    }}
                  />
                }
                label={item.label}
              />
            ))}
        </FormGroup>
      </FormControl>
    );
  }
}

CheckboxGroup.propTypes = {
  /** checkboxArr will need to be an array of objects, containing both a value and a label property */
  checkboxArr: PropTypes.array.isRequired,
  /** This is for the title/name of the checkboxgroup */
  title: PropTypes.string.isRequired,
  /** Boolean allowing you to decide whether you want the checkbox buttons displayed in a row or horizontally */
  row: PropTypes.bool
};

CheckboxGroup.defaultProps = {
  row: false
};

export default withStyles(styles)(CheckboxGroup);
