import React, { Component } from "react";
import PropTypes from "prop-types";
import { TextField, withStyles } from "material-ui";

const styles = theme => ({});

/** Input */
class Input extends Component {
  state = { inputVal: "" };

  handleChange = e => {
    let newValue = e.target.value;
    this.setState({ inputVal: newValue }, () => this.props.handleChange(this.props.name, newValue));
  };

  render() {
    const { inputVal } = this.state;
    const { label, name, shrink } = this.props;
    return (
      <TextField
        value={inputVal}
        onChange={this.handleChange}
        autoFocus
        name={name}
        margin="dense"
        id="name"
        label={label}
        InputLabelProps={shrink ? { shrink } : null}
        type="text"
        fullWidth
      />
    );
  }
}

Input.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  shrink: PropTypes.bool
};

Input.defaultProps = { label: false, shrink: true };

export default withStyles(styles)(Input);
