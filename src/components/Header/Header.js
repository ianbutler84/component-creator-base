import React, { Component } from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import { withStyles } from "material-ui/styles";
import Menu, { MenuItem } from "material-ui/Menu";
import IconButton from "material-ui/IconButton";
import MenuIcon from "material-ui-icons/Menu";
import AppsIcon from "material-ui-icons/Apps";
import PersonIcon from "material-ui-icons/Person";

const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  logo: {
    width: 130
  },
  spacer: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
});

/** This will be the header used throughout the SLAMR applications */
class Header extends Component {
  state = {
    anchorEl: null,
    anchorEl2: null
  };

  handleClick1 = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClick2 = event => {
    this.setState({ anchorEl2: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null, anchorEl2: null });
  };

  logout = () => {
    localStorage.clear();
    this.handleClose();
  };

  render() {
    const { classes, logo, toggleMenu, link } = this.props;
    const { anchorEl, anchorEl2 } = this.state;

    return (
      <div style={{ height: "64px" }}>
        <AppBar className={classes.appBar} position="absolute" color="secondary">
          <Toolbar>
            <IconButton
              className={classes.menuButton}
              color="inherit"
              onClick={toggleMenu}
              data-testid="toggle-menu-button"
            >
              <MenuIcon />
            </IconButton>
            <a href={link}>
              <img className={classes.logo} src={logo} alt="Metova Cybercents Logo" />
            </a>
            <div className={classes.spacer} />
            <IconButton
              aria-owns={anchorEl ? "simple-menu" : null}
              aria-haspopup="true"
              onClick={this.handleClick1}
              color="inherit"
            >
              <AppsIcon />
            </IconButton>
            <Menu id="simple-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={this.handleClose}>
              <MenuItem target="_blank" to="/" component={NavLink} onClick={this.handleClose}>
                Home
              </MenuItem>
              <MenuItem target="_blank" to="/projectdesigner/projects" component={NavLink} onClick={this.handleClose}>
                Project Designer
              </MenuItem>
              <MenuItem target="_blank" to="/appthree/home" component={NavLink} onClick={this.handleClose}>
                App Three
              </MenuItem>
            </Menu>
            <IconButton
              aria-owns={anchorEl2 ? "simple-menu2" : null}
              aria-haspopup="true"
              onClick={this.handleClick2}
              color="inherit"
            >
              <PersonIcon />
            </IconButton>
            <Menu id="simple-menu2" anchorEl={anchorEl2} open={Boolean(anchorEl2)} onClose={this.handleClose}>
              <MenuItem to="/" component={NavLink} onClick={this.logout}>
                Logout
              </MenuItem>
            </Menu>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

//region Props
Header.propTypes = {
  /** Allows you to add logic when the "3 bars" menu is clicked */
  toggleMenu: PropTypes.func.isRequired,
  /** You need to add the path of the logo as a string */
  logo: PropTypes.string.isRequired,
  /** Allows you to control the path where clicking on the logo will take you.  Defaults to "/" */
  link: PropTypes.string
};

Header.defaultProps = {
  link: "/"
};
//endregion

export default withStyles(styles)(Header);
