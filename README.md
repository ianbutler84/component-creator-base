# shared-react-components

## This repo is where we build out shared components between the different SLAMR applications

There are 2 uses for this repository.

1. To document how the components work and to create them in isolation.
2. To create a package that can easily be reused across all SLAMR apps.

## Create Components/ View Documentation Site
In order to create components or view the component documentation site you need to follow the following steps:

1.  Add this repo locally:
    * https://ianbutlermetova@bitbucket.org/cybercents/shared-react-components.git
2.  In the command line, at the root of the project, type: _npm install_
3.  Then type: _npm start_.  This will run the documentation site.
4.  Components are built inside _src/components/_
    * The filename where you create the component needs to be the same as the folder name.
    * The folder will need an index.js file with an export alias
5.  To add a description of the component, you need to add a comment above the component declaration using this format: 
    *  _/** Comment goes here */_  
6.  To add a description to the props, in the prop-types section, add comments above each individual proptype using the same format as above.
7.  In order to create examples for a component, you need to go to _src/docs/examples_.  Here you will need to create a folder with the same name as the component.  Inside that folder, you create as many examples as you want.  Each file will be a separate example.

## Use the shared-react-component package in your react project
1. You need a packagecloud.io account and you need to be a contributor on _centsnpm_ (contact Mike if you're not).
2.  You need to set your npm registry to look for packages on packagecloud before it checks npm's registry.  You do that with the following code in the command line:
    *  _npm config set registry https://packagecloud.io/Metova_CyberCENTS/centsnpm/npm/_ 
3. Then you have to add a user using your packagecloud account on the command line:
    * _npm adduser_
    * Then fill out your packagecloud login info as you are prompted.
4.  Now, in your project you can npm install --save component-creator-docs
5.  In your react file:
    *  _import { Header } from "component-creator-docs"_

## Publish package changes
Create a new branch with the changes you want.  __DO NOT PUBLISH__ unless your component is approved.  These changes can potentially affect multiple apps.  When your changes (or new component) is approved follow the following steps:

1.  Same as Step 1 above
2.  Same as Step 2 above
3.  Same as Step 3 above
4.  Now push your changes to the repo.  Get them approved by other developers.
5.  If approved, push to master, then from the master branch, run:
    * _npm publish_
6.  This will publish your changes with a new version to packagecloud.io

## Update your repo with latest updated changes in component-creator-docs
1. _npm update component-creator-docs_